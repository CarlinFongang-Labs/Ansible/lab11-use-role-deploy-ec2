# Ansible | Provisionner une instance ec2 à l'aide d'Ansible

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Nous souhaitons préparer un environnement de déploiement, rédiger le playbook nécessaire et valider son exécution, garantissant ainsi que le serveur web Apache fonctionne correctement et soit accessible de l'extérieur sur le port 80. Ce laboratoire nous permettra d'automatiser la tâche de déploiement du serveur Apache avec Ansible, tout en améliorant l'efficacité et la fiabilité des déploiements.

## Objectifs

Dans ce lab, nous allons :

- Créer deux instances : une hôte hébergeant Ansible et une instance cliente.

- Créer un dossier `templates` contenant un fichier `index.html.j2`, qui permettra d'afficher sur le site "Bienvenue sur -nom du client-".

- Nous modifierons le playbook `deploy.yml` afin :

  - D'utiliser **ansible.builtin.apt** pour installer git et wget à l'aide du module apt (au lieu de yum, car yum est utilisé pour les systèmes CentOS).

  - D'utiliser une condition afin d'installer wget et git uniquement si nous sommes sur un système CentOS.

  - De modifier le site internet par défaut en copiant un template `index.html.j2` qui devra afficher "Bienvenue sur -nom du client-", le nom du client étant récupéré dans les variables fournies par Ansible.

  - De modifier le déploiement du conteneur Apache pour qu'il monte le fichier `index.html` dans le répertoire où il lit le site internet par défaut (consulter la documentation d'Apache sur Docker Hub). Nous utiliserons la notion de montage de volume.

- Nous vérifierons enfin que le site est bien conforme.

## Prérequis
Disposer d'un machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## 1. Configuration projet ansible

## Exportation des clé publics
````bash
export AWS_ACCESS_KEY_ID='AKIA3FLDYKDIT5JNQ4EY'
export AWS_SECRET_ACCESS_KEY='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'
export AWS_SESSION_TOKEN='550e8400-e29b-41d4-a716-446655440079'
````


### 1.1. Mise à jour des rôles "requierement"

### 1.2. Mise à jour de playbook "deploy.yml"

### 1.3. Mise à jour du fichier "ansible.cfg"

### 1.4. Configuration des secrets de sécurité
On va exporter la key ID et la secret key afin de permettre à ansible d'exploiter les api aws pour provisionner de nouvelles ressources.

Dans le terminal on entre : 

````bash
export AWS_ACCESS_KEY_ID="AKIA4MTWIALYZFYB3KAZ" && export AWS_SECRET_ACCESS_KEY="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"
````

en entrant la command `env` on peut afficher les variables d'environnements

![alt text](image.png)
*la paire de clé est bien exportée*

## 1.5. Installation du rôle "geerlingguy.docker"

````bash
ansible-galaxy install -r roles/requirements.yml
````

![alt text](image-1.png)
*Extraction du rôle réussi*

## 1.6. Deploiement du playbook

````bash
ansible-playbook deploy.yml --ask-become-pass -vvv

ou 

ansible-playbook deploy.yml -K -vvv
````

